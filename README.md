# Splitic

Splitic is an opinionated Go test runner for CI environments that can:

- Split tests for parallelism across different worker nodes based on timing information from previous runs.
- Output and merge junit test reports.
- Output and merge coverage reports.

## Usage

`splitic test` supports different timing information providers:

- **junit**

  `splitic test -provider junit --junit-filename junit.xml` allows you to specify a junit file to load
  from local disk for timing information.

- **gitlab**
  
  Using the [Test Report API](https://docs.gitlab.com/ee/api/pipelines.html#get-a-pipelines-test-report)
  GitLab will be used if `splitic` is run within the GitLab CI environment. Alternatively, it can be configured
  at the command line using `-provider gitlab`. Using `splitic test -provider --help` will show the various
  `-gitlab-*` options available to configure the provider (endpoint, branch to use for timing information etc.)

  For private projects, an API key can be provided via a file variable with `SPLITIC_GITLAB_TOKEN_PATH` or
  a regular variable with `SPLITIC_GITLAB_TOKEN`.

  The API endpoint, branch, project ID and pipeline creation date is taken from the environment variables
  `CI_API_V4_URL`, `CI_DEFAULT_BRANCH`, `CI_PROJECT_ID`, `CI_PIPELINE_CREATED_AT`. These are predefined
  variables when running `splitic` from a GitLab pipeline. To override these, each can also be prefixed
  with `SPLITIC_GITLAB_`. For example, to override the project ID, `SPLITIC_GITLAB_CI_PROJECT_ID` will be
  preferred over `CI_PROJECT_ID`.

- **none**

  `splitic test -provider none` will not use timing information. Tests will be distributed evenly.

Tests are split deterministically into different buckets. The amount of buckets is equal to
`CI_NODE_TOTAL` or `-node-total`. The bucket chosen for any given worker node is specified through
`CI_NODE_INDEX` or `-node-index`.
