// +build with_flaky

package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"testing"
)

func testFlaky(t *testing.T, name string, count int) {
	dir := filepath.Join(os.TempDir(), "splitic-flakey")
	os.MkdirAll(dir, 0777)
	path := filepath.Join(dir, name)

	data, err := ioutil.ReadFile(path)
	if err != nil {
		ioutil.WriteFile(path, []byte("1"), 0777)
		t.Errorf("written to %v attempts: %d/%d", path, 1, count)
		return
	}

	attempts, _ := strconv.Atoi(string(data))
	if attempts < count {
		attempts++
		ioutil.WriteFile(path, []byte(strconv.Itoa(attempts)), 0777)
		t.Errorf("written to %v attempts: %d/%d", path, attempts, count)
		return
	}

	os.Remove(path)
}

func TestFlaky(t *testing.T) {
	testFlaky(t, "splitic-flaky", 2)
}

func TestFlakyNested(t *testing.T) {
	t.Run("fail-1", func(t *testing.T) {
		testFlaky(t, "splitic-flaky-fail-1", 1)
	})

	t.Run("fail-2", func(t *testing.T) {
		testFlaky(t, "splitic-flaky-fail-2", 2)
	})

	t.Run("fail-3", func(t *testing.T) {
		testFlaky(t, "splitic-flaky-fail-3", 3)
	})
}
