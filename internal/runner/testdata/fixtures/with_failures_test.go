// +build with_failures

package main

import "testing"

func TestFailure(t *testing.T) {
	t.Fail()
}

func TestFailureWithReason(t *testing.T) {
	t.Fatal("reason")
}
