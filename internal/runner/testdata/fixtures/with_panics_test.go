// +build with_panics

package main

import "testing"

func TestWithPanic(t *testing.T) {
	panic("panic during test")
}

func TestEclipsedByPanic(t *testing.T) {
	t.Fatal("unseen when panic")
}
