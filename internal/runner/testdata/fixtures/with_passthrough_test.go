// +build with_passthrough

package main

import (
	"os"
	"testing"
)

func TestEnvironmentPassthrough(t *testing.T) {
	if _, ok := os.LookupEnv("ENV_PASSTHROUGH"); !ok {
		t.Fatal("Passthrough variable does not exist")
	}

	if _, ok := os.LookupEnv("ENV_FILTERED"); ok {
		t.Fatal("Filtered variable exists")
	}
}
