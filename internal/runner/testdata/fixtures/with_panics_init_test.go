// +build with_panics_init

package main

import "testing"

func init() {
	panic("panic during init")
}

func TestEclipsedByPanicInit(t *testing.T) {
	t.Fatal("unseen when panic after init")
}
