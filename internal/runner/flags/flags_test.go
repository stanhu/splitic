package flags

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	_ "gitlab.com/ajwalker/splitic/internal/timings/junit"
)

func init() {
	testingMode = true
}

func TestParse(t *testing.T) {
	tests := map[string]struct {
		name        string
		options     Options
		coverMode   string
		goTestFlags []string
	}{
		"-provider none": {
			name: "none",
			options: Options{
				WorkingDirectory: ".",
				OutputDirectory:  ".",
				TestFailExitCode: 1,
				NodeIndex:        1,
				NodeTotal:        1,
				FlakyRetries:     3,
				PkgList:          []string{},
				CoverReport:      "cover.profile",
				JUnitReport:      "junit.xml",
			},
			coverMode:   "count",
			goTestFlags: []string{"test", "-json"},
		},
		"-provider junit": {
			name: "junit",
			options: Options{
				WorkingDirectory: ".",
				OutputDirectory:  ".",
				TestFailExitCode: 1,
				NodeIndex:        1,
				NodeTotal:        1,
				FlakyRetries:     3,
				PkgList:          []string{},
				CoverReport:      "cover.profile",
				JUnitReport:      "junit.xml",
			},
			coverMode:   "count",
			goTestFlags: []string{"test", "-json"},
		},
		"-fail-exit-code 99 -node-index 2 --node-total 3 -flaky-retries 1 -cover-report cover_1.profile -cover -coverpkg all -junit-report junit_1.xml -race -tags tag1,tag2 -debug pkg1 pkg2 pkg3 -- -count 1": {
			name: "none",
			options: Options{
				WorkingDirectory: ".",
				OutputDirectory:  ".",
				TestFailExitCode: 99,
				NodeIndex:        2,
				NodeTotal:        3,
				FlakyRetries:     1,
				Cover:            true,
				CoverPkg:         "all",
				Race:             true,
				Tags:             "tag1,tag2",
				Debug:            true,
				PkgList:          []string{"pkg1", "pkg2", "pkg3"},
				CoverReport:      "cover_1.profile",
				JUnitReport:      "junit_1.xml",
				goTestFlags:      []string{"-count", "1"},
			},
			coverMode:   "atomic",
			goTestFlags: []string{"test", "-json", "-tags", "tag1,tag2", "-race", "-covermode", "atomic", "-coverprofile", "cover_0_1.profile", "-coverpkg", "all", "-count", "1"},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			provider, options := Parse("splitic", strings.Fields(tn), ioutil.Discard)

			if tc.name != provider.Name() {
				t.Errorf("expected provider name %s, got %s", tc.name, provider.Name())
			}

			if tc.coverMode != options.CoverMode() {
				t.Errorf("expected cover mode %s, got %s", tc.coverMode, options.CoverMode())
			}

			if !reflect.DeepEqual(tc.options, options) {
				t.Errorf("expected options %+v, got %+v", tc.options, options)
			}

			if !reflect.DeepEqual(tc.goTestFlags, options.GoTestFlags("0_1")) {
				t.Errorf("expected test flags %+v, got %+v", tc.goTestFlags, options.GoTestFlags("0_1"))
			}
		})
	}
}

func TestEnvPassthrough(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ioutil.WriteFile(filepath.Join(dir, "env_1"), []byte("KEY_1\nKEY_2"), 0777)
	ioutil.WriteFile(filepath.Join(dir, "env_2"), []byte("KEY_3\nKEY_4"), 0777)
	ioutil.WriteFile(filepath.Join(dir, "env_3"), []byte("KEY_5\nKEY_6"), 0777)

	t.Run("not-found", func(t *testing.T) {
		var sb strings.Builder
		_, _ = Parse("splitic", strings.Fields("-env-passthrough unknown"), &sb)

		expected := "-env-passthrough: open unknown: no such file or directory"
		if !strings.Contains(sb.String(), expected) {
			t.Errorf("expected string to contain %q, got %q", expected, sb.String())
		}
	})

	t.Run("single", func(t *testing.T) {
		_, options := Parse(
			"splitic",
			[]string{
				"-env-passthrough", filepath.Join(dir, "env_1"),
			},
			ioutil.Discard,
		)

		expected := FileEntries{"KEY_1", "KEY_2"}
		if !reflect.DeepEqual(options.EnvPassthrough, expected) {
			t.Errorf("expected env passthrough %+v, got %+v", expected, options.EnvPassthrough)
		}
	})

	t.Run("multiple", func(t *testing.T) {
		_, options := Parse(
			"splitic",
			[]string{
				"-env-passthrough", filepath.Join(dir, "env_1"),
				"-env-passthrough", filepath.Join(dir, "env_2"),
				"-env-passthrough", filepath.Join(dir, "env_3"),
			},
			ioutil.Discard,
		)

		expected := FileEntries{"KEY_1", "KEY_2", "KEY_3", "KEY_4", "KEY_5", "KEY_6"}
		if !reflect.DeepEqual(options.EnvPassthrough, expected) {
			t.Errorf("expected env passthrough %+v, got %+v", expected, options.EnvPassthrough)
		}
	})
}

func TestQuarantined(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ioutil.WriteFile(filepath.Join(dir, "allowed_to_fail.1"), []byte("TestOne\nTestTwo"), 0777)
	ioutil.WriteFile(filepath.Join(dir, "allowed_to_fail.2"), []byte("TestOne\nTestThree"), 0777)

	t.Run("not-found", func(t *testing.T) {
		var sb strings.Builder
		_, _ = Parse("splitic", strings.Fields("-quarantined unknown"), &sb)

		expected := "-quarantined: open unknown: no such file or directory"
		if !strings.Contains(sb.String(), expected) {
			t.Errorf("expected string to contain %q, got %q", expected, sb.String())
		}
	})

	t.Run("single", func(t *testing.T) {
		_, options := Parse(
			"splitic",
			[]string{
				"-quarantined", filepath.Join(dir, "allowed_to_fail.1"),
			},
			ioutil.Discard,
		)

		expected := FileEntries{"TestOne", "TestTwo"}
		if !reflect.DeepEqual(options.Quarantined, expected) {
			t.Errorf("expected %+v, got %+v", expected, options.Quarantined)
		}

		for _, name := range expected {
			if !options.Quarantined.Has(name) {
				t.Errorf("expected %s to be present", name)
			}
		}
	})

	t.Run("multiple", func(t *testing.T) {
		_, options := Parse(
			"splitic",
			[]string{
				"-quarantined", filepath.Join(dir, "allowed_to_fail.1"),
				"-quarantined", filepath.Join(dir, "allowed_to_fail.2"),
			},
			ioutil.Discard,
		)

		expected := FileEntries{"TestOne", "TestTwo", "TestOne", "TestThree"}
		if !reflect.DeepEqual(options.Quarantined, expected) {
			t.Errorf("expected %+v, got %+v", expected, options.Quarantined)
		}

		for _, name := range expected {
			if !options.Quarantined.Has(name) {
				t.Errorf("expected %s to be present", name)
			}
		}
	})
}
