package runner

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"testing"

	"gitlab.com/ajwalker/splitic/internal/reports/junit"
	"gitlab.com/ajwalker/splitic/internal/runner/flags"
	"gitlab.com/ajwalker/splitic/internal/timings"
)

type suiteExpected struct {
	succeeded []string
	skipped   []string
	failures  []string
}

func checkTest(t *testing.T, dir string, expected ...suiteExpected) {
	defer os.RemoveAll(dir)

	ts, err := junit.Load(filepath.Join(dir, "junit.xml"))
	if err != nil {
		t.Fatal(err)
	}

	if len(ts.Suites) != len(expected) {
		t.Fatalf("expected %d test suite, but got %d", len(expected), len(ts.Suites))
	}

	for idx, suite := range ts.Suites {
		ex := expected[idx]

		var succeeded, skipped, failed []string
		for _, tc := range suite.TestCases {
			switch {
			case tc.Skipped != "":
				skipped = append(skipped, tc.Name)
			case len(tc.Error) > 0, len(tc.Failure) > 0:
				failed = append(failed, tc.Name)
			default:
				succeeded = append(succeeded, tc.Name)
			}
		}

		if !reflect.DeepEqual(succeeded, ex.succeeded) {
			t.Errorf("succeeded: expected %+v, got %+v", ex.succeeded, succeeded)
		}
		if !reflect.DeepEqual(skipped, ex.skipped) {
			t.Errorf("skipped: expected %+v, got %+v", ex.skipped, skipped)
		}
		if !reflect.DeepEqual(failed, ex.failures) {
			t.Errorf("failed: expected %+v, got %+v", ex.failures, failed)
		}
	}
}

func TestRunOnFixturesSuccess(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeTotal:        2,
		JUnitReport:      "junit.xml",
		Tags:             "with_skipped,with_ambiguous",
	}

	options.NodeIndex = 1
	if err := Run(timings.Report{}, options, nil); err != nil {
		t.Errorf("unexpected test run error: %v", err)
	}
	checkTest(t, dir,
		suiteExpected{
			[]string{"TestNested", "TestNested/a", "TestNested/b", "TestNested/c", "TestSimple"},
			[]string{"TestSkip"},
			nil,
		},
		suiteExpected{
			[]string{"TestSimple"},
			nil,
			nil,
		},
	)

	options.NodeIndex = 2
	if err := Run(timings.Report{}, options, nil); err != nil {
		t.Errorf("unexpected test run error: %v", err)
	}
	checkTest(t, dir,
		suiteExpected{
			[]string{"TestSimple"},
			[]string{"TestSkipWithReason"},
			nil,
		},
	)
}

func TestRunOnFixturesFailure(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeTotal:        2,
		JUnitReport:      "junit.xml",
		Tags:             "with_skipped,with_failures",
	}

	options.NodeIndex = 1
	if err := Run(timings.Report{}, options, nil); !errors.Is(err, ErrRunHadFailures) {
		t.Errorf("expected test run error %v, got %v", ErrRunHadFailures, err)
	}
	checkTest(t, dir,
		suiteExpected{
			[]string{"TestNested", "TestNested/a", "TestNested/b", "TestNested/c"},
			[]string{"TestSkip"},
			[]string{"TestFailure"},
		},
	)

	options.NodeIndex = 2
	if err := Run(timings.Report{}, options, nil); !errors.Is(err, ErrRunHadFailures) {
		t.Errorf("expected test run error %v, got %v", ErrRunHadFailures, err)
	}
	checkTest(t, dir,
		suiteExpected{
			[]string{"TestSimple"},
			[]string{"TestSkipWithReason"},
			[]string{"TestFailureWithReason"},
		},
	)
}

func TestRunOnFixturesPanic(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_panics",
	}

	options.NodeIndex = 1
	if err := Run(timings.Report{}, options, nil); !errors.Is(err, ErrRunWasTruncated) {
		t.Errorf("expected test run error %v, got %v", ErrRunWasTruncated, err)
	}
}

func TestRunOnFixturesPanicInit(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_panics_init",
	}

	buf := new(strings.Builder)
	config := &Config{
		Stdout: buf,
		Stderr: buf,
	}

	options.NodeIndex = 1
	if err := Run(timings.Report{}, options, config); !errors.Is(err, ErrRunWasTruncated) {
		t.Errorf("expected test run error %v, got %v", ErrRunWasTruncated, err)
	}

	if !strings.Contains(buf.String(), "panic during init") {
		t.Errorf("expected to find string 'panic during init' in output")
	}
}

func TestRunWithCoverage(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	for _, enabled := range []bool{false, true} {
		options := flags.Options{
			WorkingDirectory: "testdata/fixtures",
			OutputDirectory:  dir,
			PkgList:          []string{"./..."},
			NodeIndex:        1,
			NodeTotal:        1,
			Cover:            enabled,
			CoverReport:      "cover.profile",
			JUnitReport:      "junit.xml",
			Tags:             "with_skipped",
		}

		if err := Run(timings.Report{}, options, nil); err != nil {
			t.Errorf("unexpected test run error: %v", err)
		}

		_, err := os.Stat(filepath.Join(dir, "cover.profile"))
		if err != nil && enabled {
			t.Errorf("expected cover profile to exist: %v", err)
		}
		if err == nil && !enabled {
			t.Errorf("expected cover profile to not exist: %v", err)
		}

		os.RemoveAll(dir)
	}
}

func TestRunWithPassthrough(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeIndex:        1,
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_passthrough",
		EnvPassthrough:   []string{"GOCACHE", "HOME"},
	}

	// fail due to no passthrough
	os.Setenv("ENV_PASSTHROUGH", "allowed")
	os.Setenv("ENV_FILTERED", "not allowed")
	if err := Run(timings.Report{}, options, nil); err == nil {
		t.Error("expected test run error")
	}

	// succeed with passthrough
	options.EnvPassthrough = append(options.EnvPassthrough, "ENV_PASSTHROUGH")
	if err := Run(timings.Report{}, options, nil); err != nil {
		t.Errorf("unexpected test run error: %v", err)
	}

	// fail due to filtered variable now being allowed
	options.EnvPassthrough = append(options.EnvPassthrough, "ENV_FILTERED")
	if err := Run(timings.Report{}, options, nil); err == nil {
		t.Error("expected test run error")
	}
}

func TestRunWithQuarantined(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeIndex:        1,
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_failures",
	}

	// fail due to not allowing the failure
	if err := Run(timings.Report{}, options, nil); err == nil {
		t.Error("expected test run error")
	}

	// fail due to not allowing all failures (with fully qualified name)
	options.Quarantined = append(options.Quarantined, "gitlab.com/ajwalker/splitic/internal/runner/testdata/fixtures TestFailure")
	if err := Run(timings.Report{}, options, nil); err == nil {
		t.Error("expected test run error")
	}

	// succeed with allowed failures
	options.Quarantined = append(options.Quarantined, "TestFailureWithReason")
	if err := Run(timings.Report{}, options, nil); err != nil {
		t.Errorf("unexpected test run error: %v", err)
	}
}

func TestRunWithFlaky(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	defer os.RemoveAll(filepath.Join(os.TempDir(), "splitic-flakey"))

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeIndex:        1,
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_failures,with_flaky",
		FlakyRetries:     3,
	}

	options.Quarantined = []string{"TestFlaky"}
	options.Flaky = []string{"TestFailure", "TestFailureWithReason"}

	func() {
		defer os.RemoveAll(filepath.Join(os.TempDir(), "splitic-flakey"))

		// fail due to flaky test always failing
		if err := Run(timings.Report{}, options, nil); err == nil {
			t.Error("expected test run error")
		}
	}()

	options.Quarantined = []string{"TestFailure", "TestFailureWithReason", "TestFlakyNested/fail-3"}
	options.Flaky = []string{"TestFlaky", "TestFlakyNested/fail-1", "TestFlakyNested/fail-2"}

	func() {
		defer os.RemoveAll(filepath.Join(os.TempDir(), "splitic-flakey"))

		// succeed
		if err := Run(timings.Report{}, options, nil); err != nil {
			t.Errorf("unexpected test run error: %v", err)
		}
	}()
}

func TestRunWithMalformedOutput(t *testing.T) {
	if strings.HasPrefix(runtime.Version(), "go1.20") {
		t.Skip("go1.20 handles malformed output better")
	}

	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	options := flags.Options{
		WorkingDirectory: "testdata/fixtures",
		OutputDirectory:  dir,
		PkgList:          []string{"./..."},
		NodeIndex:        1,
		NodeTotal:        1,
		JUnitReport:      "junit.xml",
		Tags:             "with_malformed",
	}

	buf := new(bytes.Buffer)
	if err := Run(timings.Report{}, options, &Config{
		Stdout: buf,
		Stderr: buf,
	}); err != nil {
		t.Errorf("unexpected test run error: %v", err)
	}

	if !strings.Contains(buf.String(), malformedTestOutputWarning) {
		t.Errorf("expected output to contain malformed test output warning")
	}
}
