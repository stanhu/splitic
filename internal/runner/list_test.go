package runner

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

func testCompareListWithGoTest(t *testing.T, tags string) {
	curdir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	_, pathname, _, _ := runtime.Caller(0)
	rel, err := filepath.Rel(curdir, filepath.Dir(pathname))
	if err != nil {
		t.Fatal(err)
	}

	pkgs := "./" + filepath.Join(rel, "testdata", "fixtures", "...")

	// go test -list Test -json ./testdata/fixtures/...
	var goTests []testcase
	{
		buf := new(bytes.Buffer)
		cmd := exec.Command("go", "test", "-tags", tags, "-list", "Test", "-json", pkgs)
		cmd.Stdout = buf
		err := cmd.Run()
		if err != nil {
			t.Fatal(err)
		}

		dec := json.NewDecoder(buf)
		for {
			var e event
			if err := dec.Decode(&e); err != nil {
				if err == io.EOF {
					break
				}
				t.Fatal(err)
			}

			if strings.HasPrefix(e.Output, "Test") {
				goTests = append(goTests, testcase{
					pkg:  e.Package,
					name: strings.TrimSpace(e.Output),
				})
			}
		}
	}

	sortTestcases(goTests)

	listTests, err := list(curdir, []string{"-tags", tags}, []string{pkgs})
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(listTests, goTests) {
		t.Errorf("expected %+v, got %+v", goTests, listTests)
	}
}

func TestListWithGoTestList(t *testing.T) {
	testCompareListWithGoTest(t, "")
	testCompareListWithGoTest(t, "with_skipped")
	testCompareListWithGoTest(t, "with_failures")
	testCompareListWithGoTest(t, "with_skipped,with_failures")
}

func TestSortTestcases(t *testing.T) {
	testcases := []testcase{
		{pkg: "z", name: "b"},
		{pkg: "a", name: "d"},
		{pkg: "z", name: "a"},
		{pkg: "a", name: "d"},
		{pkg: "a", name: "c"},
	}

	expected := []testcase{
		{pkg: "a", name: "c"},
		{pkg: "a", name: "d"},
		{pkg: "a", name: "d"},
		{pkg: "z", name: "a"},
		{pkg: "z", name: "b"},
	}

	sortTestcases(testcases)

	if !reflect.DeepEqual(testcases, expected) {
		t.Errorf("expected %v, got %v", expected, testcases)
	}
}
