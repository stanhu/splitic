package runner

import (
	"reflect"
	"testing"

	"gitlab.com/ajwalker/splitic/internal/timings"
)

func TestBucketSimpleRun(t *testing.T) {
	buckets := make(Buckets, 1)

	report := timings.Report{}
	buckets.Add(report, testcase{pkg: "simple", name: "method_timed"})
	buckets.Add(report, testcase{pkg: "simple", name: "method_untimed"})

	buckets.Add(report, testcase{pkg: "simple_1", name: "method_unique_1_1"})
	buckets.Add(report, testcase{pkg: "simple_1", name: "method_unique_1_2"})

	buckets.Add(report, testcase{pkg: "simple_2", name: "method_unique_2_1"})
	buckets.Add(report, testcase{pkg: "simple_2", name: "method_unique_2_2"})

	expected := []RunGroup{
		{
			Packages: []string{"simple", "simple_1", "simple_2"},
			Run:      []string{"method_timed", "method_unique_1_1", "method_unique_1_2", "method_unique_2_1", "method_unique_2_2", "method_untimed"},
		},
	}

	if !reflect.DeepEqual(buckets[0].RunGroups(), expected) {
		t.Errorf("expected %+v, got %+v\n", expected, buckets[0].RunGroups())
	}
}

func TestBucketAmbigiousRun(t *testing.T) {
	buckets := make(Buckets, 1)

	report := timings.Report{}
	buckets.Add(report, testcase{pkg: "ambigious_1", name: "method_same"})
	buckets.Add(report, testcase{pkg: "ambigious_2", name: "method_same"})

	buckets.Add(report, testcase{pkg: "ambigious_1", name: "method_unique_1_1"})
	buckets.Add(report, testcase{pkg: "ambigious_1", name: "method_unique_1_2"})
	buckets.Add(report, testcase{pkg: "ambigious_1", name: "method_unique_1_3"})
	buckets.Add(report, testcase{pkg: "ambigious_1", name: "method_unique_1_4"})

	buckets.Add(report, testcase{pkg: "ambigious_2", name: "method_unique_2_1"})
	buckets.Add(report, testcase{pkg: "ambigious_2", name: "method_unique_2_2"})
	buckets.Add(report, testcase{pkg: "ambigious_2", name: "method_unique_2_3"})
	buckets.Add(report, testcase{pkg: "ambigious_2", name: "method_unique_2_4"})

	expected := []RunGroup{
		{
			Packages: []string{"ambigious_1"},
			Run:      []string{"method_same", "method_unique_1_1", "method_unique_1_2", "method_unique_1_3", "method_unique_1_4"},
		},
		{
			Packages: []string{"ambigious_2"},
			Run:      []string{"method_same", "method_unique_2_1", "method_unique_2_2", "method_unique_2_3", "method_unique_2_4"},
		},
	}

	if !reflect.DeepEqual(buckets[0].RunGroups(), expected) {
		t.Errorf("expected %+v, got %+v\n", expected, buckets[0].RunGroups())
	}
}

func TestBucketTimings(t *testing.T) {
	report := timings.Report{
		{Package: "timed", Method: "method_1s", Timing: 1},
		{Package: "timed", Method: "method_5s", Timing: 5},
		{Package: "timed", Method: "method_10s", Timing: 10},
	}

	buckets := make(Buckets, 3)

	buckets.Add(report, testcase{pkg: "timed", name: "method_1s"})
	buckets.Add(report, testcase{pkg: "timed", name: "method_5s"})
	buckets.Add(report, testcase{pkg: "timed", name: "method_10s"})
	buckets.Add(report, testcase{pkg: "untimed", name: "method_a"})
	buckets.Add(report, testcase{pkg: "untimed", name: "method_b"})
	buckets.Add(report, testcase{pkg: "untimed", name: "method_c"})
	buckets.Add(report, testcase{pkg: "untimed", name: "method_d"})

	expected := []RunGroup{
		{
			Packages: []string{"timed", "untimed"},
			Run:      []string{"method_1s", "method_a", "method_b", "method_c", "method_d"},
		},
		{
			Packages: []string{"timed"},
			Run:      []string{"method_5s"},
		},
		{
			Packages: []string{"timed"},
			Run:      []string{"method_10s"},
		},
	}

	for idx := range buckets {
		if !reflect.DeepEqual(buckets[idx].RunGroups(), []RunGroup{expected[idx]}) {
			t.Errorf("expected %+v, got %+v\n", expected[idx], buckets[idx].RunGroups())
		}
	}
}

func TestBucketTimingSelect(t *testing.T) {
	report := timings.Report{
		{Package: "d", Method: "method", Timing: 10},
		{Package: "c/d", Method: "method", Timing: 15},
		{Package: "a/b/c/d", Method: "method", Timing: 5},
		{Package: "a/b/c/d", Method: "method", Timing: 8},
	}

	buckets := make(Buckets, 1)

	buckets.Add(report, testcase{pkg: "a/b/c/d", name: "method"})

	if buckets[0].time != 8 {
		t.Errorf("expected time to be 8, got %v", buckets[0].time)
	}
}
